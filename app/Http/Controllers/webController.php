<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use DateTime;
use Illuminate\Database\QueryException;
use PHPExcel; 
use PHPExcel_IOFactory;
use PHPExcel_Shared_Date;
use PHPExcel_Style_NumberFormat;
use PHPExcel_Style_Alignment;
date_default_timezone_set('Europe/Madrid');
ini_set('display_errors', 'On');


class webController extends Controller
{
    /**
     * Display a listing of the resource.
    *  @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
       $provider = new \League\OAuth2\Client\Provider\GenericProvider([
        'clientId'                => 'bfe1a9b8-538a-4ce0-887d-cbe39ca14495',
        'clientSecret'            => 'dmHZBD669_flnzmTXE76!!;',
        'redirectUri'             => 'https://ozcheckin.azurewebsites.net',
        'urlAuthorize'            => 'https://login.microsoftonline.com/common/oauth2/v2.0/authorize',
        'urlAccessToken'          => 'https://login.microsoftonline.com/common/oauth2/v2.0/token',
        'urlResourceOwnerDetails' => '',
        'scopes'                  => 'offline_access openid user.Read'
    ]);

    if (!$request->has('code')) {
        return redirect($provider->getAuthorizationUrl());
    } else {

        try {

                $accessToken = $provider->getAccessToken('authorization_code', [
                'code'     => $request->input('code')
                    ]);
    
            
        } catch (\League\OAuth2\Client\Provider\Exception\IdentityProviderException $e) {
            // Failed to get the access token or user details.
        return redirect($provider->getAuthorizationUrl());
        }

          
   
    $client = new \GuzzleHttp\Client();

    //obtener id del usuario
$respuesta = $client->request('GET', 'https://graph.microsoft.com/v1.0/me?$select=displayName,userPrincipalName', [
    'headers' => [
        'Authorization' => 'Bearer ' . $accessToken->getToken(),
        'Content-Type' => 'application/json'
    ]
]);

 $usuario = json_decode($respuesta->getBody());

 //return($usuario->{"displayName"});
 //
 $registros = DB::table('registros_check')->where("Usuario","=",$usuario->{"userPrincipalName"})->orderBy("Fecha","DESC")->get();

 return view('/home', ['nombre'=> $usuario->{"displayName"},'userPrincipalName' => $usuario->{"userPrincipalName"}, 'registros' => $registros]);

    }
}


public function administracion(Request $request)
    {


         $provider = new \League\OAuth2\Client\Provider\GenericProvider([
        'clientId'                => 'bfe1a9b8-538a-4ce0-887d-cbe39ca14495',
        'clientSecret'            => 'dmHZBD669_flnzmTXE76!!;',
        'redirectUri'             => 'https://ozcheckin.azurewebsites.net/administracion',
        'urlAuthorize'            => 'https://login.microsoftonline.com/common/oauth2/v2.0/authorize',
        'urlAccessToken'          => 'https://login.microsoftonline.com/common/oauth2/v2.0/token',
        'urlResourceOwnerDetails' => '',
        'scopes'                  => 'offline_access openid user.Read'
    ]);

    if (!isset($_GET["code"])) {
        return redirect($provider->getAuthorizationUrl());
    } else {

        try {

                $accessToken = $provider->getAccessToken('authorization_code', [
                'code'     => $request->input('code')
                    ]);
    
            
        } catch (\League\OAuth2\Client\Provider\Exception\IdentityProviderException $e) {
            // Failed to get the access token or user details.
        return redirect($provider->getAuthorizationUrl());
        }

          
   
    $client = new \GuzzleHttp\Client();

    //obtener id del usuario
$respuesta = $client->request('GET', 'https://graph.microsoft.com/v1.0/me?$select=displayName,userPrincipalName', [
    'headers' => [
        'Authorization' => 'Bearer ' . $accessToken->getToken(),
        'Content-Type' => 'application/json'
    ]
]);

 $usuario = json_decode($respuesta->getBody());


 //return($usuario->{"displayName"});
 //
 $registros = DB::table('registros_check')->orderBy("Fecha","DESC")->get();

 //$estados = DB::table('registros_check')->selectRaw("Usuario,max(Fecha),Tipo")->groupBy(["Usuario","Tipo"])->orderBy("Fecha","DESC")->limit(1)->get();

 $estados = DB::table(DB::raw("registros_check t1"))->select(DB::raw("t1.*"))->whereRaw("t1.Fecha = (select max(t2.Fecha) from registros_check t2 where t1.Usuario = t2.Usuario)")->get();

 if($usuario->{"userPrincipalName"} != 'aag@ozein.es' && $usuario->{"userPrincipalName"} != "fgg@ozein.es" && $usuario->{"userPrincipalName"} != "avi@ozein.es" && $usuario->{"userPrincipalName"}!= "tltmb@ozein.es"){
    return view('/home', ['nombre'=> $usuario->{"displayName"},'userPrincipalName' => $usuario->{"userPrincipalName"}, 'registros' => $registros]);
 }


 return view('/administracion', ['nombre'=> $usuario->{"displayName"},'userPrincipalName' => $usuario->{"userPrincipalName"}, 'registros' => $registros,'estados'=>$estados]);

    }
}

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function filtrar(Request $request)
    {
        $output="<p style='text-align:center;'>No se han encontrado resultados.</p>";
        

            if($request->ajax()){

            $filtros = array();

            $consulta = "";

              if($request->fecha_inicio != null && $request->fecha_fin !=null){

                $fecha_inicio = $request->fecha_inicio;
                $fecha_fin = $request->fecha_fin;

                $fecha = "(Fecha BETWEEN '".$fecha_inicio."' AND '".$fecha_fin."')";

                $filtros[] = $fecha;

                //$sanciones=gesanc_sanciones::whereRaw($consulta)->get();
              
            }elseif($request->fecha_inicio == null && $request->fecha_fin !=null){
                
                $fecha_fin = $request->fecha_fin;

                $fecha = "(Fecha <= '".$fecha_fin."')";

                $filtros[] = $fecha;
            }elseif($request->fecha_inicio != null && $request->fecha_fin ==null){
                $fecha_inicio = $request->fecha_inicio;
                
                $fecha = "(Fecha >= '".$fecha_inicio."')";

                $filtros[] = $fecha;
            }

             if($request->tipo != null){
                $tipo = $request->tipo;
                $filtro_tipo = "Tipo = '".$tipo."'";
                $filtros[] = $filtro_tipo;
            } 

            if($request->accion != null){
                $accion = $request->accion;
                $filtro_accion = "Tipo = '".$accion."'";
                $filtros[] = $filtro_accion;
            } 

             if($request->usuario != null){
                $usuario = $request->usuario;
                $filtro_usuario = "Usuario like '%".$usuario."%'";
                $filtros[] = $filtro_usuario;
            } 

            for ($i =count($filtros) - 1; $i >= 0 ; $i--) { 
                $consulta .= $filtros[$i];
                $consulta .= " ";

                if($i > 0){
                    $consulta .= "AND ";
                }
            }

            //DB::enableQueryLog();
            
             $registros = DB::table('registros_check')->orderBy("Fecha","DESC")->get();

              //dd(DB::getQueryLog());
            
            if($consulta !=""){

                 $registros = DB::table('registros_check')->whereRaw($consulta)->orderBy("Fecha","DESC")->get();
             }
            
           if($registros){
                foreach ($registros as $key => $registro) {

                    if($registro->Tipo == "check-in"){
                    $output .= '<tr style="color:#5CB85C">';
                    }else{
                    $output .= '<tr style="color:#F0AD4E">';
                    }

                    $output.='<td>'.$registro->Usuario.'</td>'.
                            '<td>'.date('d - m - Y H:i', strtotime($registro->Fecha)).'</td>'.
                            '<td>'.$registro->Accion.'</td>'.
                            '<td>'.$registro->Tipo.'</td>';
                            $output.=  '<td>'.$registro->Observaciones.'</td>'.
                            '</tr>';    

                }
                return Response($output);
            }
    }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         if($request->ajax()){

            $datos = array();
            parse_str($request->datos,$datos);

            if($datos["observaciones"]!=null){
                $observaciones = $datos["observaciones"];
            }else{
                $observaciones = "";
            }

            $horas_in = 0;

            $horas_out = 0;

            $ultimo_registro = DB::table("registros_check")->whereRaw("Usuario = '".$datos["usuario"]."' and date(Fecha) = CURDATE()")->orderBy("Fecha","DESC")->first();

            $registro_horas = DB::table("tiempo_activo")->whereRaw("Usuario = '".$datos["usuario"]."' and date(Fecha) = CURDATE()")->orderBy("Fecha","DESC")->first();

            if($registro_horas == null){

                try {

                                DB::table('tiempo_activo')->insert(
                                array('Fecha' => date("Y-m-d H:i:s"),
                                      'Usuario' => $datos["usuario"]));
                                
                                } catch (QueryException $e) {
                                    return  Response($e->getMessage());
                                }
            }else{

                $horas_in = $registro_horas->tiempo_in;
                $horas_out = $registro_horas->tiempo_out;

            }

            if($ultimo_registro == null){
                 try {

                    DB::table('registros_check')->insert(
                                array('Tipo' => $datos["checkradio"],
                                        'Accion' => $datos["accion"],
                                        'Observaciones' => $observaciones,
                                        'Fecha' => date("Y-m-d H:i:s"),
                                        'Usuario' => $datos["usuario"]));

                                return  Response("Se ha registrado correctamente");
                                
                                } catch (QueryException $e) {
                                    return  Response($e->getMessage());
                                }
            }
            elseif($ultimo_registro->Tipo == $datos["checkradio"]){
                return  Response("Duplicado");

            }
            elseif($ultimo_registro->Tipo == 'check-in'){

                    $segundos = strtotime("now") - strtotime($ultimo_registro->Fecha);

                    DB::table("tiempo_activo")->whereRaw("Usuario = '".$datos["usuario"]."' and date(Fecha) = CURDATE()")->update(['tiempo_in' => $horas_in+$segundos]); 

                    try {

                                DB::table('registros_check')->insert(
                                array('Tipo' => $datos["checkradio"],
                                        'Accion' => $datos["accion"],
                                        'Observaciones' => $observaciones,
                                        'Fecha' => date("Y-m-d H:i:s"),
                                        'Usuario' => $datos["usuario"]));

                                return  Response("Se ha registrado correctamente");
                                
                                } catch (QueryException $e) {
                                    return  Response($e->getMessage());
                                }


            }else{

                    $segundos = strtotime("now") - strtotime($ultimo_registro->Fecha);

                    DB::table("tiempo_activo")->whereRaw("Usuario = '".$datos["usuario"]."' and date(Fecha) = CURDATE()")->update(['tiempo_out' => $horas_out+$segundos]); 


                  try {

                                DB::table('registros_check')->insert(
                                array('Tipo' => $datos["checkradio"],
                                        'Accion' => $datos["accion"],
                                        'Observaciones' => $observaciones,
                                        'Fecha' => date("Y-m-d H:i:s"),
                                        'Usuario' => $datos["usuario"]));

                                return  Response("Se ha registrado correctamente");
                                
                                } catch (QueryException $e) {
                                    return  Response($e->getMessage());
                                }
                }
            }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function exportar(Request $request)
    {
        $filtros = array();

            $consulta = "";

              if($request->fecha_inicio != null && $request->fecha_fin !=null){

                $fecha_inicio = $request->fecha_inicio;
                $fecha_fin = $request->fecha_fin;

                $fecha = "(Fecha BETWEEN '".$fecha_inicio."' AND '".$fecha_fin."')";

                $filtros[] = $fecha;

                //$sanciones=gesanc_sanciones::whereRaw($consulta)->get();
              
            }elseif($request->fecha_inicio == null && $request->fecha_fin !=null){
                
                $fecha_fin = $request->fecha_fin;

                $fecha = "(Fecha <= '".$fecha_fin."')";

                $filtros[] = $fecha;
            }elseif($request->fecha_inicio != null && $request->fecha_fin ==null){
                $fecha_inicio = $request->fecha_inicio;
                
                $fecha = "(Fecha >= '".$fecha_inicio."')";

                $filtros[] = $fecha;
            }

             if($request->tipo != null){
                $tipo = $request->tipo;
                $filtro_tipo = "Tipo = '".$tipo."'";
                $filtros[] = $filtro_tipo;
            } 

             if($request->usuario != null){
                $usuario = $request->usuario;
                $filtro_usuario = "Usuario like '%".$usuario."'%";
                $filtros[] = $filtro_usuario;
            } 

            for ($i =count($filtros) - 1; $i >= 0 ; $i--) { 
                $consulta .= $filtros[$i];
                $consulta .= " ";

                if($i > 0){
                    $consulta .= "AND ";
                }
            }

            //DB::enableQueryLog();
            
             $registros = DB::table('registros_check')->orderBy("Fecha","DESC")->get();

              //dd(DB::getQueryLog());
            
            if($consulta !=""){

                 $registros = DB::table('registros_check')->whereRaw($consulta)->orderBy("Fecha","DESC")->get();
             }

             $objPHPExcel = new PHPExcel();
        // Se asignan las propiedades del libro
        $objPHPExcel->getProperties()->setCreator("OZEIN") // Nombre del autor
        ->setLastModifiedBy("OZEIN") //Ultimo usuario que lo modificó
        ->setTitle("Historial Checkinout") // Titulo
        ->setDescription("Historial Checkinout"); //Descripción
        $titulosColumnas = array('Usuario'
                                ,'Fecha'
                                ,'Tipo'
                                ,'Accion'
                                ,'Observaciones');
        
        // Se combinan las celdas, para colocar ahí el titulo del reporte
        //$objPHPExcel->setActiveSheetIndex(0)
        //->mergeCells('A1:D1');
        $objPHPExcel->setActiveSheetIndex(0)
        ->setCellValue('A1', $titulosColumnas[0])
        ->setCellValue('B1', $titulosColumnas[1])
        ->setCellValue('C1', $titulosColumnas[2])
        ->setCellValue('D1', $titulosColumnas[3])
        ->setCellValue('E1', $titulosColumnas[4]);

        $i=2;
        foreach ($registros as $key => $registro) {
              $objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A'.$i, $registro->Usuario)
            ->setCellValue('B'.$i, PHPExcel_Shared_Date::PHPToExcel(strtotime($registro->Fecha)))
            ->setCellValue('C'.$i, $registro->Tipo)
            ->setCellValue('D'.$i, $registro->Accion)
            ->setCellValue('E'.$i, $registro->Observaciones);
            $objPHPExcel->getActiveSheet()->getStyle('B'.$i)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_DATE_DDMMYYYY);
            $i++;
        }



    header('Content-Encoding: UTF-8');
    header('Content-type: text/csv; charset=UTF-8');
    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    header('Content-Disposition: attachment;filename="historial checkin.xlsx"');
    header('Cache-Control: max-age=0');
    header("Pragma: no-cache");
    header("Expires: 0");
    header('Content-Transfer-Encoding: binary');
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        $objWriter->save('php://output');
   
    }

     public function exportar_horas(Request $request)
    {
       

             $objPHPExcel = new PHPExcel();
        // Se asignan las propiedades del libro
        $objPHPExcel->getProperties()->setCreator("OZEIN") // Nombre del autor
        ->setLastModifiedBy("OZEIN") //Ultimo usuario que lo modificó
        ->setTitle("Horas activas") // Titulo
        ->setDescription("Horas activas"); //Descripción
        $titulosColumnas = array('Fecha'
                                ,'Usuario'
                                ,'tiempo_in'
                                ,'tiempo_out');
        
        // Se combinan las celdas, para colocar ahí el titulo del reporte
        //$objPHPExcel->setActiveSheetIndex(0)
        //->mergeCells('A1:D1');
        $objPHPExcel->setActiveSheetIndex(0)
        ->setCellValue('A1', $titulosColumnas[0])
        ->setCellValue('B1', $titulosColumnas[1])
        ->setCellValue('C1', $titulosColumnas[2])
        ->setCellValue('D1', $titulosColumnas[3]);

        $horas = DB::table("tiempo_activo")->orderBy("Fecha","DESC");

        $i=2;
        foreach ($horas as $key => $registro) {

            $horas_in = $registro->tiempo_in/60;
            $horas_in /= 60;

            $horas_out = $registro->tiempo_out/60;
            $horas_out /= 60;

              $objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A'.$i, PHPExcel_Shared_Date::PHPToExcel(strtotime($registro->Fecha)))
            ->setCellValue('B'.$i, $registro->Usuario)
            ->setCellValue('C'.$i, $horas_in)
            ->setCellValue('D'.$i, $horas_out);
            $objPHPExcel->getActiveSheet()->getStyle('A'.$i)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_DATE_DDMMYYYY);
            $i++;
        }



    header('Content-Encoding: UTF-8');
    header('Content-type: text/csv; charset=UTF-8');
    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    header('Content-Disposition: attachment;filename="Horas_activas.xlsx"');
    header('Cache-Control: max-age=0');
    header("Pragma: no-cache");
    header("Expires: 0");
    header('Content-Transfer-Encoding: binary');
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        $objWriter->save('php://output');
   
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function pintar_tabla(Request $request)
    {
        if($request->ajax()){
            $output = "";
            $registros = DB::table('registros_check')->where("Usuario","=",$request->usuario)->orderBy("Fecha","DESC")->get();
            if($registros){
                foreach ($registros as $key => $registro) {

                    if($registro->Tipo == "check-in"){
                    $output .= '<tr style="color:#5CB85C">';
                    }else{
                    $output .= '<tr style="color:#F0AD4E">';
                    }

                    $output.='<td>'.date('d - m - Y H:i', strtotime($registro->Fecha)).'</td>'.
                            '<td>'.$registro->Tipo.'</td>'.
                            '<td>'.$registro->Accion.'</td>';
                    $output.=  '<td>'.$registro->Observaciones.'</td>'.
                            '</tr>';    

                }
                return Response($output);
            }

        }
    }

   


}
