<?php

namespace App\Http\Middleware;

use Closure;

class IpMiddleware
{

    public function handle($request, Closure $next)
    {
        if ($request->ip() != "91.126.73.225") {
        // here insted checking single ip address we can do collection of ip 
        //address in constant file and check with in_array function
            return redirect('error');
        }

        return $next($request);
    }

}
?>