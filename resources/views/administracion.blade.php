@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row">
        <div class="col-sm-2">
            <h4>Buscar</h4>
            <div class =filtros>
            
                 <a data-toggle="collapse" href="#fechallegada" aria-expanded="false" aria-controls="fechallegada"><h5 class="titulo_filtro"><i class="fa fa-plus"></i> Fecha</h5></a>
                <div class="collapse" id="fechallegada">
                <input type="text" class="form-control datepicker" name="fecha_inicio" id="fecha_inicio" value="" placeholder="fecha de inicio...">
                <br>
                <input type="text" class="form-control datepicker" id="fecha_fin" value="" placeholder="fecha de fin...">
                </div>
            </div>
            <div class="filtros">
                 <a data-toggle="collapse" href="#collapseexpe" aria-expanded="false" aria-controls="collapseexpe"><h5 class="titulo_filtro"><i class="fa fa-plus"></i> Usuario</h5></a>
                 <div class="collapse" id="collapseexpe">
             <input type="text" class="form-control" id="usuario" name="usuario" placeholder="Buscar...">
             </div>
            </div>
            <div class="filtros">
                 <a data-toggle="collapse" href="#collapseboletin" aria-expanded="false" aria-controls="collapseboletin"><h5 class="titulo_filtro"><i class="fa fa-plus"></i> Tipo</h5></a>
                 <div class="collapse" id="collapseboletin">
             <select class="form-control" name="tipo" id="tipo">
                 <option value="" selected></option>
                 <option value="check-in">check-in</option>
                <option value="check-out">check-out</option>
             </select>
             </div>
            </div>
            <div class="filtros">
                 <a data-toggle="collapse" href="#collapseaccion" aria-expanded="false" aria-controls="collapseaccion"><h5 class="titulo_filtro"><i class="fa fa-plus"></i> Acción</h5></a>
                 <div class="collapse" id="collapseaccion">
                <select class="form-control" name="accion">
                 <option value="jornada">Jornada</option>
                <option value="descanso">Descanso</option>
            <option value="Almuerzo">Almuerzo</option>
            </select>
             </div>
            </div>
            <div class="filtros" style="margin-top:5px">
            <button type="button" id="filtrar" class="btn btn-default btn-primary">Filtrar</button>
            </div>
        </div>
        <div class="col-sm-7">
            <div class="panel panel-default">
                <div class="row"> 
                    <div class="panel-heading" id="sanciones-header">                    
                        <div class="col-md-10">
                            <h3 class="module-title"><i class="fa fa-table" aria-hidden="true"></i> Historial completo</h3>
                        </div>
                      
                    </div>
                </div>
                <div class="row"> 
                    <div class="panel-heading" id="sanciones-header">                    
                        <div class="col-md-10">
                            <div>
                                <ul class="list-inline">
                                    <li><a href="#" id="exportar"><i class="fa fa-file-excel-o" aria-hidden="true"></i> Exportar historial</a></li>
                                    <li><a href="#" id="exportar_horas"><i class="fa fa-file-excel-o" aria-hidden="true"></i> Exportar Tiempo activo</a></li>
                                </ul>
                            </div>
                        </div>
                        </div>
                    </div>
                <div class="table-responsive">
                <table class="table">
                        <thead class="table-header">
                            <th>Usuario</th>
                            <th>Fecha</th>
                            <th>Tipo</th>
                            <th>Acción</th>                          
                            <th>Descanso</th>
                            <th>Observaciones</th>
                        </thead>
                        <tbody id="myTable">
                            @foreach ($registros as $registro)
                            @if($registro->Tipo == "check-in")
                            <tr style="color:#5CB85C">
                            @else
                            <tr style="color:#F0AD4E">
                            @endif
                            <td>{{ $registro->Usuario }} </td>
                            <td>{{ date('d - m - Y H:i', strtotime($registro->Fecha)) }} </td>
                            <td>{{ $registro->Tipo }} </td>
                            <td>{{ $registro->Accion }} </td>
                            @if($registro->Descanso == 0)
                            <td>NO</td>
                            @else
                            <td>SI</td>
                            @endif
                            <td>{{ $registro->Observaciones }} </td>
                            </tr>    
                            @endforeach
                        </tbody>
                    </table>
              </div>
              <div class="col-md-12 text-center">
                  <ul class="pagination pagination-lg pager" id="myPager"></ul>
                  </div>

            </div>
        </div>
        <div class="col-sm-3">
            <div class="panel panel-default">
              <div class="row"> 
                 <div class="panel-heading" id="sanciones-header">                    
                        <div class="col-md-10">
                            <h3 class="module-title"><i class="fa fa-users" aria-hidden="true"></i> Estado</h3>
                        </div>
                    </div>
            </div>
             <div class="panel-body" style="border-top:2px solid #00C1DE;">
            <ul class="list-group">
                @foreach($estados as $estado)
                @if($estado->Tipo == "check-in")
              <li class="list-group-item" style="color:#5CB85C !important"><i class="fa fa-check-circle" aria-hidden="true"></i> {{$estado->Usuario}}</li>
              @else
              <li class="list-group-item" style="color:#F0AD4E !important"><i class="fa fa-times-circle" aria-hidden="true"></i> {{$estado->Usuario}}</li>
              @endif
                @endforeach
            </ul>
            </div>
        </div>
           
        </div>
    </div>
</div>

<script type="text/javascript">

$('#myTable').pageMe({pagerSelector: '#myPager', showPrevNext: true, hidePageNumbers: false, perPage: 10});

 $('.datepicker').datepicker({
        format: "dd/mm/yyyy",
        dateFormat: 'yy-mm-dd',
        language: "es",
        autoclose: true
    });
    

$( "#filtrar" ).click(function() {
$fecha_inicio = $("#fecha_inicio").val();
$fecha_fin = $("#fecha_fin").val();

$tipo = $("#tipo option:selected").text();

$usuario = $("#usuario").val();

$inicio = $fecha_inicio.split("/").reverse().join("-");
$fin = $fecha_fin.split("/").reverse().join("-");



//alert($inicio);

        $dialog = bootbox.dialog({
                        message: '<p class="text-center">Cargando datos, espere por favor...</p>',
                        closeButton: false
                    });

    setTimeout(function(){
            $.ajax({
                type : 'get',
                url  : '{{URL::to('/filtrar')}}',
                data : {'fecha_inicio':$inicio,'fecha_fin': $fin,'tipo':$tipo,'usuario': $usuario},            
                success : function(data){
                    console.log(JSON.stringify(data));
                    $('tbody').html(data);

                    // do something in the background
                    $dialog.modal('hide');
                    $("#loading").hide();
                },
                error : function(data){
                    console.log(JSON.stringify(data));
                }
            });
        }, 500);

});


$( "#exportar" ).click(function() {
$fecha_inicio = $("#fecha_inicio").val();
$fecha_fin = $("#fecha_fin").val();

$tipo = $("#tipo option:selected").text();

$accion = $("#accion option:selected").text();

$usuario = $("#usuario").val();

$inicio = $fecha_inicio.split("/").reverse().join("-");
$fin = $fecha_fin.split("/").reverse().join("-");



//alert($inicio);

        $dialog = bootbox.dialog({
                        message: '<p class="text-center">Cargando datos, espere por favor...</p>',
                        closeButton: false
                    });

    setTimeout(function(){
            $.ajax({
                type : 'get',
                url  : '{{URL::to('/exportar')}}',
                data : {'fecha_inicio':$inicio,'fecha_fin': $fin,'tipo':$tipo,'usuario': $usuario,'accion': $accion},            
                success : function(data){
                    window.location = this.url;
                    $dialog.modal('hide');
                },
                error : function(data){
                    //console.log(JSON.stringify(data));
                    bootbox.alert("Ha ocurrido un error generando su Excel.");
                    $dialog.modal('hide');
                }
            });
        }, 500);

});

$( "#exportar_horas" ).click(function() {

        $dialog = bootbox.dialog({
                        message: '<p class="text-center">Cargando datos, espere por favor...</p>',
                        closeButton: false
                    });

    setTimeout(function(){
            $.ajax({
                type : 'get',
                url  : '{{URL::to('/exportar_horas')}}',        
                success : function(data){
                    window.location = this.url;
                    $dialog.modal('hide');
                },
                error : function(data){
                    //console.log(JSON.stringify(data));
                    bootbox.alert("Ha ocurrido un error generando su Excel.");
                    $dialog.modal('hide');
                }
            });
        }, 500);

});
</script>
@endsection