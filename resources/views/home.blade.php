@extends('layouts.app') @section('content')
<div class="container">
    <div class="row">
        <div class="col-sm-12">
            <div class="alert alert-danger alert-dismissable" style="display: none !important">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
            <strong>¡Error detectado!</strong> No puede realizar un registro del mismo tipo dos veces consecutivas.
          </div>
          <div class="alert alert-success alert-dismissable" style="display: none !important">
              <a href="#" class="close" data-dismiss="alert" aria-label="close">x</a>
              <strong>¡Su check se ha registrado correctamente!</strong>
            </div>   
        </div>
        <div class="col-sm-5">
            <div class="panel panel-default">
                <div class="row">
                    <div class="panel-heading" id="sanciones-header">
                        <div class="col-md-10">
                            <h3 class="module-title"><i class="fa fa-check-square-o" aria-hidden="true"></i> Registrar entrada/salida</h3>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="panel-heading" id="sanciones-header">
                        <div class="col-md-10">
                            <div>
                                <ul class="list-inline">
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="panel-body" style="border-top:2px solid #00C1DE;">
                    <form class="inline-form" id="form_checkin">
                        <div class="form-group row">
                            <div class="col-md-12">
                                <label>Tipo</label>
                                <ul class="list-inline">
                                    <li class="list-inline-item">
                                        <input type="radio" name="checkradio" id="checkin" value="check-in" checked> Check-IN
                                    </li>
                                    <li class="list-inline-item">
                                        <input type="radio" name="checkradio" id="checkout" value="check-out"> Check-OUT
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="form-group row">
                        <div class="col-md-12">
                                <label>Acción</label>
                                <ul class="list-inline">
                                   <select class="form-control" name="accion">
                                       <option value="jornada">Jornada</option>
                                       <option value="descanso">Descanso</option>
                                       <option value="Almuerzo">Almuerzo</option>
                                   </select>
                                </ul>
                            </div>
                        </div>
                         <div class="form-group row">
                            <div class="col-md-12">
                                <label>Observaciones</label><br>
                                <textarea rows="4" cols="50" class="form-control" name="observaciones"></textarea>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-12">
                                <input type="hidden" name="usuario" id="usuario" value="{{$userPrincipalName}}">
                                <input type="button" name="submit" id="submit" class="btn btn-info" value="Enviar">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-sm-7">
            <div class="panel panel-default">
                <div class="row">
                    <div class="panel-heading" id="sanciones-header">
                        <div class="col-md-10">
                            <h3 class="module-title"><i class="fa fa-table" aria-hidden="true"></i> Historial</h3>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="panel-heading" id="sanciones-header">
                        <div class="col-md-10">
                            <div>
                                <ul class="list-inline">
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="panel-body" style="border-top:2px solid #00C1DE;">
                <div class="table-responsive">
                <table class="table">
                        <thead class="table-header">
                            <th>Fecha</th>
                            <th>Tipo</th>
                            <th>Acción</th>                         
                            <th>Observaciones</th>
                        </thead>
                        <tbody id="myTable">
                            @foreach ($registros as $registro)
                            @if($registro->Tipo == "check-in")
                            <tr style="color:#5CB85C">
                            @else
                            <tr style="color:#F0AD4E">
                            @endif
                            <td>{{ date('d - m - Y H:i', strtotime($registro->Fecha)) }} </td>
                            <td>{{ $registro->Tipo }} </td>
                            <td>{{ $registro->Accion }} </td>
                            <td>{{ $registro->Observaciones }} </td>
                            </tr>    
                            @endforeach
                        </tbody>
                    </table>
              </div>
              <div class="col-md-12 text-center">
                  <ul class="pagination pagination-lg pager" id="myPager"></ul>
                  </div>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">

$('#myTable').pageMe({pagerSelector: '#myPager', showPrevNext: true, hidePageNumbers: false, perPage: 10});

$("#submit").click(function(){

    var dialog = bootbox.dialog({
    message: '<p class="text-center">Registrando...</p>',
    closeButton: false
});
// do something in the background


    $form = $("#form_checkin").serialize();
  
     setTimeout(function(){
            $.ajax({
                type : 'get',
                url  : '{{URL::to('/store')}}',
                data : {'datos' : $("#form_checkin").serialize()},            
                success : function(data){
                    console.log(JSON.stringify(data));
                    if(data == "Duplicado"){
                            $( ".alert-danger" ).show();
                            $( ".alert-success" ).hide();
                            dialog.modal('hide');
                    }else{
                            $( ".alert-danger" ).hide();
                            $( ".alert-success" ).show();
                            dialog.modal('hide');
                            $.ajax({
                                    type : 'get',
                                    url  : '{{URL::to('/pintar_tabla')}}',
                                    data : {'usuario' : $("#usuario").val()},            
                                    success : function(data){
                                        $("#myTable").empty();
                                        $("#myTable").append(data);
                                        //$('#myTable').pageMe({pagerSelector: '#myPager', showPrevNext: true, hidePageNumbers: false, perPage: 10});
                                    },
                                    error : function(data){
                                        console.log(JSON.stringify(data));
                                        bootbox.alert({
                                                message: "Se ha producido un error, por favor inténtelo más tarde.",
                                                callback: function () {
                                                    //location.reload();
                                                }
                                            });
                                    }
                                });
                    }
                    

                },
                error : function(data){
                    console.log(JSON.stringify(data));
                }
            });
        }, 500);

});
        

    </script>
    @endsection