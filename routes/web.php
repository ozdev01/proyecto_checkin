<?php

use Illuminate\Http\Request;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
    return view('welcome');
});*/

/*Route::get('/','webController@index');

Route::get('/administracion','webController@administracion');

Route::get('/filtrar','webController@filtrar');

Route::get('/exportar','webController@exportar');

Route::get('/store','webController@store');

Route::get('/pintar_tabla','webController@pintar_tabla');*/

Route::get('/', ['middleware' => ['ipcheck'], 'uses' => 'webController@index']);
Route::get('/administracion', ['middleware' => ['ipcheck'],'uses' =>'webController@administracion']);
Route::get('/filtrar', ['middleware' => ['ipcheck'],'uses' => 'webController@filtrar']);
Route::get('/exportar', ['middleware' => ['ipcheck'],'uses' => 'webController@exportar']);
Route::get('/exportar_horas', ['middleware' => ['ipcheck'],'uses' => 'webController@exportar_horas']);
Route::get('/store', ['middleware' => ['ipcheck'],'uses' => 'webController@store']);
Route::get('/pintar_tabla', ['middleware' => ['ipcheck'],'uses' => 'webController@pintar_tabla']);

Route::get('/error', function () {
    return view('error');
});





